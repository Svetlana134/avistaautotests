package pages;

import org.openqa.selenium.By;


public class Common extends PageNew {

    public String LOGIN_PAGE_URL = "https://a5preprod.avistatime.com";


    //-----Main Menu-----//
    public By customersMainMenu = By.cssSelector("a[webix_l_id='customers']");
    public By allCustomersCustomersMainMenu = By.cssSelector("a[webix_l_id='customers_all']");
    public By planningMainMenu = By.cssSelector("a[webix_l_id='planning']");
    public By shiftSchedulePlanningMainMenu = By.cssSelector("a[webix_l_id='planning_delivery_schemes']");





    public By dashboardReport = By.cssSelector(".dashboard_report");


    public  void login (String user, String password) {
        inputField( By.cssSelector(".login_form_text [type='text']"), user);
        inputField( By.cssSelector("input[type='password']"), password);
        findElementClick(By.cssSelector("button[type='button']"));
    }

    public String getTextOfItemAndClick(By element) {
        String text = getTextOfElement(element);
        findElementClick(element);
        return text;
    }


}
