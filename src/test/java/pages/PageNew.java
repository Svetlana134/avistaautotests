package pages;

import tests.TestDescription;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.testng.Assert.assertTrue;


public class PageNew {

    private Logger LOG = Logger.getLogger(PageNew.class);
    protected WebDriver driver;


    protected ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement toReturn = driver.findElement(locator);
                if (toReturn.isDisplayed()) {
                    return toReturn;
                }
                return null;
            }
        };
    }

    protected ExpectedCondition<WebElement> availabilityOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement toReturn = driver.findElement(locator);
                if (toReturn.isEnabled()) {
                    return toReturn;
                }
                return null;
            }
        };
    }


    public void errorScreenshot() {
        try {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String path = "./resources/screenshots/" + TestDescription.getInst().getClassName() + "_" + TestDescription.getInst().getMethod() + ".png"; // screenshot.getName(); -> screenshot6132534184894364942.png
            FileUtils.copyFile(screenshot, new File(path));
        } catch (Exception e) {
            LOG.error("Error in screenshot method");
        }
    }

    //-------- Generate random value --------//

    public String randomEmailAddress() {
        Date curTime = new Date();
        DateFormat date = new SimpleDateFormat("ddMMyyyyHHmmss");
        String dateTime = date.format(curTime) + "_" + System.currentTimeMillis();
        return "email_" + dateTime + "@LogIn.LogIn";
    }


    public int randomNumber(Integer max) {return (int) (Math.random() * max + 1);}

    public String randomText(int lengthOfText) {
        String string = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        String[] abc = string.split(",");
        Integer max = abc.length;
        String text = "";
        for (int i = 0; i < lengthOfText; i++) {
            Integer number = randomNumber(max);
            text += abc[number];
        }
        return text;
    }

    public Integer chooseRandomItem(By element) {
        int numberOfElements = getSizeOfElements(element);
        return randomNumber(numberOfElements);
    }

    //-------- Getting value --------//

    public String getMethodName(String className) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        for (int i = 0; i < ste.length; i++) {
            System.out.println(ste[i].getClassName() + ", " + className + ", " + ste[i].toString());
            if (className.contains(ste[i].getClassName())) {
                return ste[i].getMethodName();
            }
        }
        return "Unknown";
    }

    public String getAttributeOfElement(final By findElement, final String attribute) {
        WebElement element = waitForElement(findElement);
        return element.getAttribute(attribute);
    }

    public String getTextOfElement(final By findElement) {
        WebElement element = waitForElement(findElement);
        return element.getText();
    }

    public Integer getSizeOfElements(final By locator) {
        List<WebElement> elements = null;
        try {
            elements = (new WebDriverWait(driver, 10)).until(new ExpectedCondition<List<WebElement>>() {
                public List<WebElement> apply(WebDriver webDriver) {
                    return webDriver.findElements(locator);
                }
            });
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + locator + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
        return elements.size();
    }

    //-------- For waiting --------//

    public void timeout(int value) {
        try {
            Thread.sleep(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public WebElement waitForElement(By element) {
        WebElement thisElement = null;
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            thisElement = wait.until(visibilityOfElementLocated(element));
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
        return thisElement;
    }

    public void waitUntilInvisibilityElement(By element) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.not(ExpectedConditions.presenceOfAllElementsLocatedBy(element)));
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error(element + "' in class '" + className + "' method '" + getMethodName(className) + "' wasn't disappeared");
            assertTrue(false);
        }
    }

    public void waitUntilPageIsLoaded() {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.onload");
        } catch (Exception e) {
            LOG.error("Page wasn't loaded");
            assertTrue(false);
        }
    }

    //-------- For actions --------//

    public void inputField(By findElement, String msg) {
        try {
            WebElement element = waitForElement(findElement);
            element.clear();
            element.sendKeys(msg);
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found input  " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void findElementClick(By findElement) {
        try {
            waitForElement(findElement).click();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void doubleClickElement(By element) {
        try {
            WebElement thisElement = waitForElement(element);
            new Actions(driver).doubleClick(thisElement).perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void dndElements(By dragElement, By targetElement) {
        try {
            WebElement thisDraggableElement = waitForElement(dragElement);
            WebElement thisTargetElement = waitForElement(targetElement);
            new Actions(driver).clickAndHold(thisDraggableElement).moveToElement(thisTargetElement).release().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Can not dnd elements in class " + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void dndElementsWithCoordinates(By element, int x, int y) {
        try {
            WebElement dragElement = waitForElement(element);
            new Actions(driver).dragAndDropBy(dragElement, x, y).release().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Can not dnd elements in class " + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void moveCursorToElement(By findElement) {
        try {
            WebElement element = waitForElement(findElement);
            new Actions(driver).moveToElement(element).build().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void scrollIntoViewOfElement(By pathToElement) {
        try {
            WebElement element = waitForElement(pathToElement);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView(true);", element);
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + pathToElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void dropDownList(By buttonForOpeningList, By itemInList) {
        try {
            WebElement element = waitForElement(buttonForOpeningList);
            Actions mouse = new Actions(driver);
            mouse.moveToElement(element).click().perform();
            WebElement item = waitForElement(itemInList);
            mouse.moveToElement(item).click().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Error in work of the dropdown list in class '" + className + "' method '" + getMethodName(className) + "'");
            errorScreenshot();
            assertTrue(false);
        }
    }

    //-------- For checks --------//

    public boolean isElementPresent(final By locator) {
        Boolean result;
        try {
            result = (new WebDriverWait(driver, 5)).until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    List<WebElement> elements = webDriver.findElements(locator);
                    return elements.size() > 0;
                }
            });
        } catch (Exception e) {
            result = false;
        }
        return result;
    }


    public Boolean isElementVisible(final By locator) {
        Boolean result;
        try {
            result = (new WebDriverWait(driver, 5)).until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    WebElement element = webDriver.findElement(locator);
                    return element.isDisplayed();
                }
            });
        } catch (Exception e) {
            result = false;
        }
        return result;
    }


    //----------Methods for working with new tab-----------//

    public void openNewTab() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.open();");
    }

    public void switchToFrame() {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("frame"));
        } catch (Exception e) {
            System.err.println("Couldn't get to frame");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void switchToDefaultContent() {
        try {
            driver.switchTo().defaultContent();
        } catch (Exception e) {
            System.err.println("Couldn't get to default content");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public String getWindowHandle() {
        return (new WebDriverWait(driver, 30)).until(new ExpectedCondition<String>() {
            public String apply(WebDriver webDriver) {
                return webDriver.getWindowHandle();
            }
        });
    }


    public String getCurrentUrl() {
        return (new WebDriverWait(driver, 30)).until(new ExpectedCondition<String>() {
            public String apply(WebDriver webDriver) {
                return webDriver.getCurrentUrl();
            }
        });
    }

    public void switchFromFirstTabToSecond(String currentWindows) {
        try {
            for (String handle : driver.getWindowHandles()) {
                if (handle != currentWindows) {
                    driver.switchTo().window(handle);
                    driver.switchTo().activeElement();
                } // смотрим все вкладки (а их две всего); если і-тая вкладка не равна первой (инициализированой в пункте (а), тогда переключаемся на нее).
            }
        } catch (Exception e) {
            System.err.println("Couldn't get to second page");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void switchFromSecondTabToFirst(String currentWindows) {
        try {
            driver.switchTo().window(currentWindows);
            driver.switchTo().activeElement();
        } catch (Exception e) {
            System.err.println("Couldn't get back to first page");
            errorScreenshot();
            assertTrue(false);
        }
    }

    public void refreshPage() {
        driver.navigate().refresh();
        waitUntilPageIsLoaded();
    }

    protected void returnToPreviousPageOfBrowser() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("history.back()");
    }

    public void goToPage(String linkOfPage) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.location.href = '" + linkOfPage + "'");
    }

    protected void goToPageInNewTab(String currentWindows, String link) {
        openNewTab();
        switchFromFirstTabToSecond(currentWindows);
        goToPage(link);
    }

    public String ramdomNumber1( Integer max) {
        // Integer max = 23000;
        Integer number1 = (int) (Math.random() * (max - 1) + 1);
        String number = number1.toString();
        return number;
    }

    // Get count of element
    public int getCountElement(By element){
        List<WebElement> list = driver.findElements(element);
        return list.size();
    }


}
