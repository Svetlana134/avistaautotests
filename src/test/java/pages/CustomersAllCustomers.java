package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class CustomersAllCustomers extends Common{

    public CustomersAllCustomers(WebDriver driver) {
        super.driver = driver;
        driver.get (LOGIN_PAGE_URL);
        login("ssavastyuk@xbsoftware.com", "12340");
    }

    //-----Main menu-----//
    public By newCustomerButton = By.cssSelector("[view_id='add_customer_button'] button[type='button']");
    public By editCustomerButton = By.cssSelector("[view_id='edit_customer_button']");
    public By pausebutton = By.cssSelector("[view_id='hold_button']");
    public By newContractButton = By.cssSelector("[view_id='add_contract_button']");
    public By editContractButton = By.cssSelector("[view_id='edit_contract_button']");
    public By avistaplacesButton = By.cssSelector("[view_id='avista_places_button']");
    public By deleteCustomerButton = By.cssSelector("[view_id='delete_customer_button']");
    public By acceptDeletionButton = By.cssSelector("[view_id='rccw_button_remove']");
    public By innactiveCustomerButton = By.cssSelector("[view_id='rccw_button_inactivate']");


    //-----New customer popup-----//
    public By firstNameInput = By.cssSelector("[view_id='customer_form_view_FirstName'] input[type='text']");
    public By lastNameInput = By.xpath("//*[contains(text(), 'Last name')]/following-sibling::input");
    public By endDateInput = By.cssSelector(".webix_el_datepicker_end .webix_inp_static");
    public By todayButtonInTheCalendar = By.cssSelector(".webix_cal_icon_today.webix_cal_icon");
    public By assignOrganisationTab = By.cssSelector("[button_id='tab_assign_organization']");
    public String firstOrganisationCheckbox = "$(\".webix_view.webix_layout_clean input:first\")";
    public By saveButton = By.cssSelector("[view_id='settings_btn_organization']~.custom_disabled");
    public By saveButtonBasicInformation = By.cssSelector("[view_id='settings_btn_basic']~.webix_el_button.custom_disabled");
    public By avistaplaceTaskAddress = By.xpath("//label[contains(text(),'Task address')]/following-sibling::input");
    public By confirmSavingWithoutGPSButton = By.cssSelector(".webix_popup_button.confirm");

    public By selectedRowCell = By.cssSelector(".webix_row_select");


    //-----Display of the columns in the Grid-----//
    public By columnSelectionButton = By.cssSelector("[view_id='gridViewButton']");
    public By selectNoneColumn = By.cssSelector("[view_id='buttonGridViewPopupUnselectAll']");
    public By selectAllColumns = By.cssSelector("[view_id='buttonGridViewPopupSelectAll']");

    //-----Pause popup-----//
    public By addButtonNewPause = By.xpath("//button[.='Add']");
    public By reasonInput = By.xpath("//label[.='Reason']/following-sibling::input");
    public By savePauseButton = By.cssSelector("[view_id='windows_hold_button_save']");
    public By cancelPauseButton = By.cssSelector("[view_id='cancel_hold_button']");

    //-----New contract popup-----//
    public By nameOfNewContractInput = By.xpath("//*[contains(text(), 'Name')]/following-sibling::input");
    public By activityTypeOfNewContractDropdown = By.xpath("//label[contains(text(),'Activity type')]/following-sibling::div");
    public By activityTypeInThedroplist (int x) {return By.xpath("//*[@webix_l_id='"+x+"']");}
    public By startDateContract = By.xpath("//label[contains(text(),'Start date')]/following-sibling::div");

    //-----Calendar-----//
    public By calendarBlock = By.xpath("//*[contains (@style, 'display: block')]//div[contains(@class,'webix_cal_day') and not (contains(@class,'outside'))]");
    public By dayCellInCalendarBlock(int x) {return By.xpath("(//*[contains (@style, 'display: block')]//div[contains(@class,'webix_cal_day') and not (contains(@class,'outside'))])[" + x + "]");}


    //-----AvistaplacePage-----//
    public By newAvistaplaceButton = By.xpath("//button[contains(text(),'New Avistaplace')]");
    public By avistaplaceTitleInput = By.xpath("//label[contains(text(),'Avistaplace')][not(contains(text(),'Hierarchy'))]/following-sibling::input");
    public By latitudeInput = By.xpath("//label[contains(text(),'Latitude')]/following-sibling::input");
    public By longitudeInput = By.xpath("//label[contains(text(),'Longitude')]/following-sibling::input");
    public By addNewAvistaplaceButton = By.cssSelector("[view_id='new_place_wnd_add_button']");
    public By backToCustomerListButton = By.cssSelector("[view_id='back_button']");
    public By copyAvistaplaceButton = By.cssSelector("[view_id='copy_button']");
    public By roomTab = By.cssSelector("[button_id='tab_locale']");
    public By roomIdInput = By.xpath("//label[contains(text(),'Room ID')]/following-sibling::input");
    public By roomSizeInput = By.xpath("//label[contains(text(),'Room size')]/following-sibling::input");
    public By deleteAvistaplaceButton = By.cssSelector("[view_id='remove_button']");
    public By acceptDeletionOfAvistaplaceButton = By.cssSelector(".confirm");
    public By managingSensorsButton = By.cssSelector("[view_id='managing_geo_position_button']");
    public By techniqueOfManagingSensorsDropdown = By.cssSelector("[view_id='window_managing_geo_position_technic']");
    public By idOfManagingSensorsDropdown = By.cssSelector("[view_id='window_managing_geo_position_technic_id']");
    public By saveTechniqueButton = By.xpath("//*[@view_id='window_managing_geo_position_technic_id']/following-sibling::div[1]");


    //-----New work order-----//
    public String newWorkOrderButton =  "$(\".webix_icon.fa-list-alt:first\")";
    public By fromTaskPeriodButton = By.cssSelector("[view_id='order_datepicker_from");
    public String todayTaskPeriodFomCalendarButton = "$(\".webix_cal_icon_today.webix_cal_icon\")";
    public String toTaskPeriodButton = "$(\".webix_input_icon.fa-calendar:last\")";
    public String todayTaskPeriodToCalendarButton = "$(\".webix_cal_icon_today.webix_cal_icon\")";
    public String CreateNewAvistaplaceButton = "$(\".webixtype_base:contains('Create new Avistaplace')\")";
    public By avistaplaceInputPopupCreateNewAvistaplace = By.xpath("(//label[contains(text(),'Avistaplace')])[1]/following-sibling::input");
    public String addButtonPopupCreateNewAvistaplace = "$(\".webix_img_btn_abs.webixtype_base:contains(' Add')\")";
    public String changeActivitiesButton = "$(\".webixtype_base:contains('Change activities')\")";
    public String selectLastItemInChangeActivities = "$(\".webix_cell .webix_tree_file:last\")";
    public String selectButtonInPopupChangeActivities = "$(\".webixtype_base:contains('Select')\")";
    public By workOrderNameInput = By.xpath("//div[contains(text(),'Work order name')]/parent::div/following-sibling::div[@class='webix_view webix_control webix_el_text']/*/input");
    public String scheduleDropDown = "$(\".webix_view.webix_control.webix_el_richselect:first .webix_el_box\")";
    public By saveWorkOrderButton = By.cssSelector("[view_id='window_order_button_disable']");
    public By okButtonConfirm = By.cssSelector(".webix_popup_button.confirm");

    //-----Order list-----//
    public String orderListButton = "$(\".webix_icon.fa-list-alt:last\")";
    public By nameWorkOrderInOrderList(String nameWorkOrder) {return By.cssSelector(".webix_cell:contains('" + nameWorkOrder + "')");}
    public String backButtonOrderList = "$(\".webix_el_box:contains('Back')\")";




    public ArrayList<String> createNewCustomer () {

       findElementClick(customersMainMenu);
       findElementClick(allCustomersCustomersMainMenu);
       timeout(1000);
       findElementClick(newCustomerButton);
       String firstNameText = "!";
       inputField(firstNameInput,firstNameText);
       timeout(3000);
       String lastNameText = ramdomNumber1(8);
       inputField(lastNameInput, lastNameText);
       String fullCustomerName = firstNameText+" "+lastNameText;
       timeout(3000);
       String avistaPlaceName = ramdomNumber1(8);
       inputField(avistaplaceTaskAddress, avistaPlaceName);

            ArrayList <String> customerAndAvistaplaceName = new ArrayList<String>();
            customerAndAvistaplaceName.add(fullCustomerName);
            customerAndAvistaplaceName.add(avistaPlaceName);


       findElementClick(assignOrganisationTab);
       ((JavascriptExecutor)driver).executeScript(firstOrganisationCheckbox+".click()");
       findElementClick(saveButton);
       findElementClick(confirmSavingWithoutGPSButton);
      // findElementClick(saveButton);

       //return fullCustomerName;
       return customerAndAvistaplaceName;
    }

    public void deleteNewCustomer (String name) {
        timeout(1000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:contains('"+name+"')\").click()");

        findElementClick(deleteCustomerButton);
        findElementClick(acceptDeletionButton);
    }
    public void innactiveNewCustomer (String name) {
        timeout(1000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:contains('"+name+"')\").click()");

        findElementClick(deleteCustomerButton);
        findElementClick(innactiveCustomerButton);
    }


   /* public By kjg = By.cssSelector(".webix_cell:contains('\" + name + \"')");
    public String locator(String name) { return "[.webix_cell:contains('" + name + "')]\")";};

    public void sdf(String name){
        waitForElement(By.cssSelector("[" + locator(name) + "]"));
        ((JavascriptExecutor)driver).executeScript("$(" + locator(name) + ").click()");
    }*/

    public void clickNewCellWithText (String name){
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:contains('"+name+"')\").click()");
    }

    public String editNewCustomer (String name) {
        clickNewCellWithText(name);
        findElementClick(editCustomerButton);
        findElementClick(endDateInput);
        findElementClick(todayButtonInTheCalendar);
        String selectedDate = getTextOfElement(endDateInput);
        findElementClick(saveButtonBasicInformation);
        return selectedDate;
    }

    public void selectEndDateDisplayInTheGrid (){

        timeout(1000);
        findElementClick(columnSelectionButton);
        findElementClick(selectNoneColumn);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:nth-child(13) .webix_table_checkbox\").click()");
    }

    public void selectStateColumnDisplayInTheGrid (String name) {
        timeout(1000);
        clickNewCellWithText(name);
        findElementClick(columnSelectionButton);
        findElementClick(selectNoneColumn);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:nth-child(18) .webix_table_checkbox\").click()");
    }
    public void selectStartDateColumnInTheGrid (){
        timeout(1000);
        findElementClick(columnSelectionButton);
        findElementClick(selectNoneColumn);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:nth-child(12) .webix_table_checkbox\").click()");
    }

    public void displayAllCoulumnsInTheGrid () {
        timeout(1000);
        findElementClick(columnSelectionButton);
        findElementClick(selectAllColumns);
    }

    public void pauseCreation (String name) {
        clickNewCellWithText(name);
        findElementClick(pausebutton);
        inputField(reasonInput, randomText(8));
        findElementClick(addButtonNewPause);
        findElementClick(savePauseButton);
    }

    public void pauseCancellation () {
        ((JavascriptExecutor)driver).executeScript("$(\"[view_id='hold_grid'] .webix_first .webix_cell\").click()");
        timeout(1000);
        findElementClick(cancelPauseButton);
        findElementClick(savePauseButton);

    }

    public String createNewContract (String name) {
        clickNewCellWithText(name);
        findElementClick(newContractButton);
        timeout(1000);
        String contractName = randomText(8);
        inputField(nameOfNewContractInput, contractName);
        findElementClick(activityTypeOfNewContractDropdown);
        findElementClick(activityTypeInThedroplist(randomNumber(2)));
        findElementClick(saveButtonBasicInformation);
        timeout(1000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_tree_close:contains('" + name + "')\").click()");
        driver.findElement(By.cssSelector(".webix_row_select .webix_tree_close")).click();
        return contractName;
    }

    public String selectDateInCalendar() {
        findElementClick(startDateContract);
        int randomDay = chooseRandomItem(calendarBlock);
        findElementClick(dayCellInCalendarBlock(randomDay));
        return getTextOfElement(startDateContract);
    }

    public String createAvistaplace () {
        findElementClick(newAvistaplaceButton);
        String newTitleForAvistaplace = randomText(8);
        inputField(avistaplaceTitleInput,newTitleForAvistaplace);
        inputField(latitudeInput,String.valueOf(randomNumber(1000000000)));
        timeout(1000);
        inputField(longitudeInput, String.valueOf(randomNumber(1000000000)));
        timeout(1000);
        findElementClick(addNewAvistaplaceButton);
        return newTitleForAvistaplace;
    }

    public String copyAvistaplace () {
        findElementClick(copyAvistaplaceButton);
        inputField(avistaplaceTitleInput, randomText(5));
        String titleOfCopiedAvistaplace = getTextOfElement(avistaplaceTitleInput);
        findElementClick(roomTab);
        inputField(roomIdInput, randomText(5));
        inputField(roomSizeInput, String.valueOf(randomNumber(50)));
        findElementClick(addNewAvistaplaceButton);
        return titleOfCopiedAvistaplace;
    }

    public void managingSensorsCreation () {
        findElementClick(managingSensorsButton);

    }

    public void selectRandomItemInDropDownSchedulePopupWorkOrder(){
        // Page page = new Page();
        // ((JavascriptExecutor)driver).executeScript("return $(\"[view_id='window_order_form']~.webix_view.webix_window.webix_popup .webix_win_content .webix_list_item\").size()")
        int count = getCountElement(By.cssSelector("[view_id='window_order_form']~.webix_view.webix_window.webix_popup .webix_win_content .webix_list_item")); //получает количество элементов в дропдауне
        String count1 = ramdomNumber1(count); //рандомно выбирает из всех элементов
        timeout(1000);
        waitForElement(By.cssSelector("[view_id='window_order_form']~.webix_view.webix_window.webix_popup .webix_win_content .webix_list_item:nth-child(" + count1 + ")")).click(); //исходя из предыдещго рандома, выбирает этот элемент из списка
    }

    public void selectContract(String titleContract){
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:contains('" + titleContract + "')\").click()");
    }

    public void findElementClickJS (String elementJS) {
        ((JavascriptExecutor)driver).executeScript(elementJS+".click()");
    }


}
