package pages;

//import tests.TestDescription;
//import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

//import static org.junit.Assert.assertTrue;

public class Page  {




    private static Logger LOG = Logger.getLogger(Page.class);
    protected WebDriver driver;

   /* public  void login (String user, String password) {
        inputField( By.cssSelector("label:contains('Login')~input[type='text']"), user);
        inputField( By.cssSelector("input[type='password']"), password);
        findElementClick(By.cssSelector("div[view_id='login_form'].button[type='button']"));
    }*/
    public String randomEmailAddres() {
        Date curTime = new Date();
        DateFormat date = DateFormat.getDateInstance();
        String dateTime = date.format(curTime) + "_" + System.currentTimeMillis();
        String email = "email_" + dateTime + "@test.test";
        return email;
    }

    public String inputField(By findElement, String msg) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 60);
            WebElement element = wait.until(visibilityOfElementLocated(findElement));
            element.clear();
            element.sendKeys(msg);

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found input  " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            //errorScreenshot();
            assertTrue(false);
        }
        return msg;
    }


    public static String ramdomNumber( Integer max) {
        // Integer max = 23000;
        Integer number1 = (int) (Math.random() * (max - 1) + 1);
        String number = number1.toString();
        return number;
    }

    public  String randomText(int lengthOfText) {
        String string = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
        String[] abc = string.split(",");
        Integer max = abc.length;
        String text = "";
        for (int i = 0; i < lengthOfText; i++) {
            Integer number = (int) (Math.random() * (max - 1) + 1);
            text += abc[number];
        }
        return text;
    }

    public void checkedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") != null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' unchecked in class '" + className + "' method '" + getMethodName(className) + "'");
            //errorScreenshot();
            assertTrue(false);
        }
    }

    public void uncheckedElements(By findElement) {
        WebElement element = driver.findElement(findElement);
        if (element.getAttribute("checked") == null) {
            String str = "ok";
        } else {
            String className = getClass().toString();
            LOG.error("Element '" + findElement + "' checked in class '" + className + "' method '" + getMethodName(className) + "'");
            //errorScreenshot();
            assertTrue(false);

        }
    }
    // Method to find elements and click

    public void findElementClick(By findElement) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(findElement));
            element.click();

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            //errorScreenshot();
            assertTrue(false);
        }
    }

    public void inputFormNewCustomer(String titleField, String text){
        if (titleField.equals("First name")){
            driver.findElement(By.cssSelector("[view_id='customer_form_view_FirstName'] input")).sendKeys(text);
        } else driver.findElement(By.xpath("//*[contains(text(), '" + titleField + "')]/following-sibling::input")).sendKeys(text);

    }

    public static String getMethodName(String className) {
        //используется внутри каждого метода
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();

        for (int i = 0; i < ste.length; i++) {
            System.out.println(ste[i].getClassName() + ", " + className + ", " + ste[i].toString());
            if (className.contains(ste[i].getClassName())) {
                return ste[i].getMethodName();
            }
        }
        return "Unknown";
    }

    public void timeout(int value) {
        try {
            Thread.sleep(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void timeout() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void selectOption(String value) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement element = wait.until(visibilityOfElementLocated(By.tagName("select")));
            List<WebElement> allSelects = driver.findElements(By.tagName("select"));
            for (WebElement select : allSelects) {
                List<WebElement> allOptions = select.findElements(By.tagName("option"));
                for (WebElement option : allOptions) {
                    if (option.getAttribute("value").contains(value)) {
                        option.click();
                        return;
                    }
                }
            }
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found select option '" + value + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            // errorScreenshot();
            assertTrue(false);
        }
    }

    protected ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement toReturn = driver.findElement(locator);
                if (toReturn.isDisplayed()) {
                    return toReturn;
                }
                return null;
            }
        };
    }

    public void waitForElement(By element) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement elementWait = wait.until(ExpectedConditions.elementToBeClickable(element));

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            //errorScreenshot();
            assertTrue(false);
        }
    }

    public void waitUntilPageIsLoaded() {
        try {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.onload");
        } catch (Exception e) {
            LOG.error("Page wasn't loaded");
            assertTrue(false);
        }
    }
  /*  WebElement element = (new WebDriverWait(driver, 10))
            .until(ExpectedConditions.elementToBeClickable(By.id("someid")))*/


    public void dndElements(By dragElement, By targetElement) {
        try {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            WebElement draggable = driver.findElement(dragElement);
            String drag = draggable.toString();
            WebElement target = driver.findElement(targetElement);
            new Actions(driver).clickAndHold(draggable).moveToElement(target).release().perform();
        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Can not dnd elements in class " + className + "' method '" + getMethodName(className) + "'");
            // errorScreenshot();
            assertTrue(false);
        }
    }

    public String getTextElement(By findElement) {
        waitForElement(findElement);
        return driver.findElement(findElement).getText();
    }


    public void moveCursorToElement(By findElement) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement element = wait.until(visibilityOfElementLocated(findElement));
            new Actions(driver).moveToElement(element).perform();

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            // errorScreenshot();
            assertTrue(false);
        }
    }

    public Integer getCountElements(By element) {
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement elementFind = wait.until(visibilityOfElementLocated(element));
            List<WebElement> located_elements = driver.findElements(element);
            int count = 0;
            for (WebElement located_element : located_elements) {
                count++;
            }
            return count;

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + element + "' in class '" + className + "' method '" + getMethodName(className) + "'");
            // errorScreenshot();
            assertTrue(false);
            return 0;
        }
    }

    public Boolean isElementPresent(By locator){
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Boolean result = driver.findElements(locator).size()>0;
        //assertTrue(result);
        return  result;
    }

    // Method to upload files
    public void uploadedFile(By findElement, String filePath) {
        try {
            WebElement element = driver.findElement(findElement);
            File file = new File(filePath);
            String fileName = file.getAbsolutePath();
            element.sendKeys(fileName);

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("File '" + filePath + "' did not upload.... in  '" + className + "' method '" + getMethodName(className) + "'");
            /*driver.close();
            driver.quit();*/
            // errorScreenshot();
            assertTrue(false);

        }
    }

    // Double click

    public void doubleClickElement(By findElement){
        try {
            Wait<WebDriver> wait = new WebDriverWait(driver, 30);
            WebElement element = wait.until(visibilityOfElementLocated(findElement));
            Actions action = new Actions(driver);
            action.click(element).build().perform();
            action.click(element).build().perform();

        } catch (Exception e) {
            String className = getClass().toString();
            LOG.error("Not found " + "'" + findElement + "' in class '" + className + "' method '" + getMethodName(className) + "'");
           /* driver.close();
            driver.quit();*/
            //errorScreenshot();
            assertTrue(false);
        }
    }

    // right click

    public void rightClickElement(By findElement){
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebElement element = driver.findElement(findElement);
        Actions action= new Actions(driver);
        action.moveToElement(element);
        //action.contextClick(element).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.RETURN).build().perform();
        action.contextClick(element).build().perform();
    }

    public void scrollDown() {
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
    }
    public void hover (By findElement) {
        Actions actions = new Actions (driver);
        WebElement element = driver.findElement(findElement);
        actions.moveToElement(element).perform();
    }

    public void scrollUp() {
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).perform();
    }
    public void pressEnter() {
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.ENTER).perform();
    }

    // Get count of element
    public int getCountElement(By element){
        List<WebElement> list = driver.findElements(element);
        return list.size();
    }
  /*  public void errorScreenshot( ){
        try {
            //String className = getClass().toString();
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String path = "./resources/screenshots/" + TestDescription.getInst().getClassName() + "_" + TestDescription.getInst().getMethod() + ".png"; // screenshot.getName(); -> screenshot6132534184894364942.png
            FileUtils.copyFile(screenshot, new File(path));
        }
        catch(Exception e)
        {}
    }*/
}

