package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static pages.Page.ramdomNumber;

public class PlanningShiftSchedule extends Common{

    public PlanningShiftSchedule(WebDriver driver) {
        super.driver = driver;
        driver.get (LOGIN_PAGE_URL);
        login("ssavastyuk@xbsoftware.com", "12340");
    }


    public By newScheduleButton = By.xpath("//button[contains(text(),' New schedule')]");
    public By weekDayWorkHours (String x) { return By.xpath("//*[@view_id='work_hours_" + x + "_1_1']"); }
    public By weekDayLunchHours (String x) {return By.xpath("//*[@view_id='lunch_hours_" + x + "_1_1']");}
    public By workHoursRange (String x) {return By.xpath("//*[@view_id='$suggest5_list']//*[@class='webix_list_item']["+x+"]");}


    public void createNewSchedule () {
        findElementClick(planningMainMenu);
        findElementClick(shiftSchedulePlanningMainMenu);
        findElementClick(newScheduleButton);
        findElementClick(weekDayWorkHours("1"));
        //findElementClick(weekDayWorkHours(ramdomNumber(7)));
        String randomWorkHoursRange = ramdomNumber(10);
        String workHoursSelected = getTextOfItemAndClick(weekDayWorkHours(randomWorkHoursRange));
        //findElementClick(workHoursRange(randomWorkHoursRange));





    }

}
