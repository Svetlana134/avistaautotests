package tests;


import pages.CustomersAllCustomers;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static com.thoughtworks.selenium.SeleneseTestBase.assertFalse;
import static com.thoughtworks.selenium.SeleneseTestBase.assertTrue;



public class CustomersAllCustomersTest extends BaseTest{

   @Test
    public void loginTest () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        customersAllCustomers.timeout(1000);
        assertTrue(customersAllCustomers.isElementPresent(customersAllCustomers.dashboardReport));
    }

    @Test
    public void AC_01_createCustomer () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(3000);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + name + "')\")");
        assertTrue(result);
        customersAllCustomers.deleteNewCustomer(name);
    }


    @Test
    public void AC_02_editCustomerEndDate () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(1000);
        String date = customersAllCustomers.editNewCustomer(name);
        customersAllCustomers.selectEndDateDisplayInTheGrid();
        customersAllCustomers.timeout(1000);
        assertTrue(customersAllCustomers.getTextOfElement(customersAllCustomers.selectedRowCell).equals(date));
        customersAllCustomers.deleteNewCustomer(name);
        customersAllCustomers.displayAllCoulumnsInTheGrid();
    }

    @Test
    public void AC_03_addPause () {
       CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
       customersAllCustomers.timeout(1000);
       customersAllCustomers.pauseCreation(name);

       driver.navigate().refresh();
       customersAllCustomers.selectStateColumnDisplayInTheGrid(name);
       customersAllCustomers.timeout(1000);

       assertTrue(customersAllCustomers.getTextOfElement(customersAllCustomers.selectedRowCell).contains("Paused"));
       customersAllCustomers.deleteNewCustomer(name);
       customersAllCustomers.displayAllCoulumnsInTheGrid();

    }

    @Test
    public void AC_04_pauseCanceled (){
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.pauseCreation(name);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.findElementClick(customersAllCustomers.pausebutton);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.pauseCancellation();
        driver.navigate().refresh();
        customersAllCustomers.selectStateColumnDisplayInTheGrid(name);
        customersAllCustomers.timeout(1000);
        assertTrue(customersAllCustomers.getTextOfElement(customersAllCustomers.selectedRowCell).isEmpty());
        customersAllCustomers.deleteNewCustomer(name);
        customersAllCustomers.displayAllCoulumnsInTheGrid();

    }

    @Test
    public void AC_05_customerContractCreation () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(2000);
        String titleConract = customersAllCustomers.createNewContract(name);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + titleConract + "')\")");
        assertTrue(result);
        customersAllCustomers.deleteNewCustomer(name);
        customersAllCustomers.displayAllCoulumnsInTheGrid();
    }

   @Test
    public void AC_06_editStartDateOfContract(){
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(2000);
        String titleConract = customersAllCustomers.createNewContract(name);
        customersAllCustomers.timeout(3000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_cell:contains('" + titleConract + "')\").click()");
        customersAllCustomers.findElementClick(customersAllCustomers.editContractButton);
        String selectedDate= customersAllCustomers.selectDateInCalendar();
        customersAllCustomers.findElementClick(customersAllCustomers.saveButtonBasicInformation);
        customersAllCustomers.selectStartDateColumnInTheGrid();
        assertTrue(customersAllCustomers.getTextOfElement(customersAllCustomers.selectedRowCell).contains(selectedDate));
        customersAllCustomers.findElementClick(customersAllCustomers.selectAllColumns);
        customersAllCustomers.deleteNewCustomer(name);
        customersAllCustomers.displayAllCoulumnsInTheGrid();

    }


    @Test
    public void AC_07_presenceOfTheAvistaplace (){
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        String customerAvistaplaceName = customerAndAvistaplaceNameList.get(1);
        customersAllCustomers.timeout(3000);
        customersAllCustomers.clickNewCellWithText(name);
        customersAllCustomers.findElementClick(customersAllCustomers.avistaplacesButton);
        customersAllCustomers.timeout(3000);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + customerAvistaplaceName + "')\")");
        assertTrue(result);
        customersAllCustomers.findElementClick(customersAllCustomers.backToCustomerListButton);
        customersAllCustomers.deleteNewCustomer(name);

    }

    @Test
    public void AC_08_creationOfNewAvistaplace () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.clickNewCellWithText(name);
        customersAllCustomers.findElementClick(customersAllCustomers.avistaplacesButton);
        customersAllCustomers.timeout(1000);
        String createdAvistaplaceTitle = customersAllCustomers.createAvistaplace();
        customersAllCustomers.timeout(1000);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + createdAvistaplaceTitle + "')\")");
        assertTrue(result);
        customersAllCustomers.findElementClick(customersAllCustomers.backToCustomerListButton);
        customersAllCustomers.deleteNewCustomer(name);

    }

    @Test
    public void AC_09_copyAvistaplace () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.clickNewCellWithText(name);
        customersAllCustomers.findElementClick(customersAllCustomers.avistaplacesButton);
        customersAllCustomers.timeout(1000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_first .webix_cell\").click()");
        String copedAvistaplacetitle = customersAllCustomers.copyAvistaplace();
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + copedAvistaplacetitle + "')\")");
        assertTrue(result);
        customersAllCustomers.findElementClick(customersAllCustomers.backToCustomerListButton);
        customersAllCustomers.deleteNewCustomer(name);

    }

   @Test
    public void AC_10_deletioOfAvistaplace () {
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        String nameAvistaplace = customerAndAvistaplaceNameList.get(1);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.clickNewCellWithText(name);
        customersAllCustomers.findElementClick(customersAllCustomers.avistaplacesButton);
        customersAllCustomers.timeout(1000);
        ((JavascriptExecutor)driver).executeScript("$(\".webix_first .webix_cell\").click()");
        customersAllCustomers.findElementClick(customersAllCustomers.deleteAvistaplaceButton);
        customersAllCustomers.findElementClick(customersAllCustomers.acceptDeletionOfAvistaplaceButton);
        customersAllCustomers.timeout(1000);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + nameAvistaplace + "')\")");
        assertFalse(result);
        customersAllCustomers.findElementClick(customersAllCustomers.backToCustomerListButton);
        customersAllCustomers.deleteNewCustomer(name);

    }

    @Test
    public void AC_17_creationOfNewWorkOrder (){
        CustomersAllCustomers customersAllCustomers = new CustomersAllCustomers(driver);
        ArrayList <String> customerAndAvistaplaceNameList=customersAllCustomers.createNewCustomer();
        String name =customerAndAvistaplaceNameList.get(0);
        customersAllCustomers.timeout(2000);
        String titleContract = customersAllCustomers.createNewContract(name);
        customersAllCustomers.selectContract(titleContract);
        customersAllCustomers.findElementClickJS(customersAllCustomers.newWorkOrderButton);
        customersAllCustomers.timeout(2000);
        customersAllCustomers.findElementClick(customersAllCustomers.fromTaskPeriodButton);
        customersAllCustomers.findElementClickJS(customersAllCustomers.todayTaskPeriodFomCalendarButton);
        customersAllCustomers.timeout(2000);
        customersAllCustomers.findElementClickJS(customersAllCustomers.toTaskPeriodButton);
        customersAllCustomers.findElementClickJS(customersAllCustomers.todayTaskPeriodToCalendarButton);
        customersAllCustomers.findElementClickJS(customersAllCustomers.changeActivitiesButton);
        customersAllCustomers.findElementClickJS(customersAllCustomers.selectLastItemInChangeActivities);
        customersAllCustomers.findElementClickJS(customersAllCustomers.selectButtonInPopupChangeActivities);
        String workOrderName= customersAllCustomers.ramdomNumber1(3);
        customersAllCustomers.inputField(customersAllCustomers.workOrderNameInput,workOrderName);
        // customersAllCustomers.findElementClick(customersAllCustomers.workOrderNameInput);
        customersAllCustomers.findElementClickJS(customersAllCustomers.scheduleDropDown);
        customersAllCustomers.timeout(3000);
        customersAllCustomers.selectRandomItemInDropDownSchedulePopupWorkOrder();
        customersAllCustomers.findElementClick(customersAllCustomers.saveWorkOrderButton);
        customersAllCustomers.findElementClick(customersAllCustomers.okButtonConfirm);
        customersAllCustomers.selectContract(titleContract);
        customersAllCustomers.timeout(1000);
        customersAllCustomers.findElementClickJS(customersAllCustomers.orderListButton);
        customersAllCustomers.timeout(1000);
        Boolean result =(Boolean) ((JavascriptExecutor)driver).executeScript("return $(\"div\").is(\".webix_cell:contains('" + workOrderName + "')\")");
        assertTrue(result);
        customersAllCustomers.findElementClickJS(customersAllCustomers.backButtonOrderList);
        customersAllCustomers.innactiveNewCustomer(name);


    }

    @Test
    public void AC_12_addingManagingSensors () {

    }





}
