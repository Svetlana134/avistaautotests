package tests;

//import junit.framework.TestCase;
//import junit.framework.TestCase;
import org.apache.xerces.util.URI;
//import org.apache.xmlbeans.impl.xb.ltgfmt.TestCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;


public class BaseTest {
    protected WebDriver driver;

    @BeforeMethod
    //выполняется первым
    public void setUp () throws URI.MalformedURIException {
        System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath ());
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        //driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown () throws Exception {
        driver.close();
    }
}
//123