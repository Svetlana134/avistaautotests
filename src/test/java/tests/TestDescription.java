package tests;

public class TestDescription {

    private String className;
    private String method;

    private static TestDescription testDescription = null;



    public void setMethod(String method) {
        this.method = method;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethod() {
        return method;
    }

    public String getClassName() {
        return className;
    }

    public static TestDescription getInst() {
        if(testDescription == null) {
            testDescription = new TestDescription();
        }
        return testDescription;
    }

}
